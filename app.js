var express = require('express');
var pdu = require('./pdu.js');
var app = express();



var ip = 'localhost';
var port = '1301'

//app.use(express.bodyParser());
app.use(express.urlencoded())
app.use(express.json())

app.get('/', function(req, res) {
    res.type('text/plain');
    res.send('PDU Encode and Decode Server is running');
});

app.post('/encode', function(req, res) {
  if(!req.body.hasOwnProperty('message') || 
     !req.body.hasOwnProperty('phone') || 
     !req.body.hasOwnProperty('smsc') || 
     !req.body.hasOwnProperty('size')) {
    res.statusCode = 400;
    return res.send('Error 400: Post syntax incorrect.');
  }
  var message = req.body.message;
  var phone = req.body.phone;
  var smsc = req.body.smsc;
  var size = req.body.size;

  console.log("requst params:");
  console.log(req.body);

  //check size value
  if(size == 16) {     
      if(message.length > 70) {
           message = message.substring(0,70);
           console.log("truncated:"+message);
      }     
  } else if (size == 7 || size == 8) {
       if(message.length > 140) {
           message = message.substring(0,140);
           console.log("truncated:"+message);
      }  
  } else {
      res.statusCode = 400;
      return res.send('Error 400: size value error.');
  }

  
  var result = pdu.pdu(message,phone,smsc,size);
  console.log("pdu return:");
  console.log(result);
  console.log("\n");
  res.json(result);
});

app.post('/decode', function(req, res) {
  if(!req.body.hasOwnProperty('pdu')) {
    res.statusCode = 400;
    return res.send('Error 400: Post syntax incorrect.');
  }
  var paramPDU = req.body.pdu;  

  console.log("requst params:");
  console.log(req.body);
  
  var result = pdu.decodePDU(paramPDU);
  console.log("pdu return:");
  console.log(result);
  console.log("\n");
  res.json(result);
});

app.listen(port ||6666,ip);
console.log("http://" + ip + ":" + (port ||6666));


